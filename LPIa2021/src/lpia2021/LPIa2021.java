/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lpia2021;

/**
 *
 * @author edgar
 */
public class LPIa2021 {
    
    // Varial¡bles de alcance público
    public static     String sMensaje = "Lenguaje de Programación I, Grupo A";
    public static     String sError   = "Mensaje con sentancia Falsa";
    public static     boolean linea = false;
    public static     int valor1 = 2;
    public static     int valor2 = 1;
    public static     String sMsg1 = "Igual";
    public static     String sMsg2 = "Menor";
    
    // Método para imprimir los encabezados
    public static void encabezado() {
        System.out.println("Universidad Autonóma de Campeche");
        System.out.println("Facultad de Ingeniería");
        System.out.println("Ingeniería de Sistemas Computacionales");
        System.out.println("Abril 2021");
    }

    public static void cuerpo() {
        System.out.println("=========================================================");
        System.out.println("Seleccione una Opción para efectuar ");
        System.out.println("");
        System.out.println("1 if Simple y Compuesto");
        System.out.println("2 if Operadore Relacionale");
        System.out.println("3 if Operadores Relacionales Igualdad");
        System.out.println("4 if Anidado Extendido");
        System.out.println("5 if Anidado Implicito");
        System.out.println("6 Ciclos For");
        System.out.println("=========================================================");
    }

    public static void pie() {
        System.out.println("---------------------------------------------------------");
        System.out.println("EDC.2021");
    }

    public static void ifSimpleyCompuesto() {

        System.out.println("=============[If Simple y Compuesto]==========================");        
        if (linea) {
            System.out.println(sMensaje);
        } else {
            System.out.println(sError);           
        }       
    }

    public static void ifOperadoreRelacionales() {
        System.out.println("=============[Operadores Relacionales]==========================");
        
        if (valor1 > valor2) {
            System.out.println(sMensaje);
        } else {
            System.out.println(sError);           
        }       
    }

    public static void ifOpRelacionalesIgualdad() {
        System.out.println("=============[Operadores Relacionales Igualdad]==========================");
        
        valor1 = 2;
        valor2 = 1;
        if (valor1 == valor2) {
            System.out.println(sMensaje);
        } else {
            System.out.println(sError);           
            
        }
    }
    public static void ifAnidadoExtendido() {
        System.out.println("=============[If Anidado Extendido]==========================");
        valor1 = 1;
        valor2 = 2;
        
        sMensaje = "Mayor";     
        if (valor1 > valor2) {
            System.out.println(sMensaje);
        } else {
            if (valor1 == valor2){
               System.out.println(sMsg1);           
            }else{
               System.out.println(sMsg2);           
            }            
        }
    }

    public static void ifAnidadoImplicito() {
        System.out.println("=============[If Anidado Implicito]==========================");
        valor1 = 1;
        valor2 = 1;
        
        sMensaje = "Mayor";
        sMsg1 = "Igual";
        sMsg2 = "Menor";
        
        if (valor1 > valor2) {
            System.out.println(sMensaje);
        } else if (valor1 == valor2){
               System.out.println(sMsg1);           
        }else{
               System.out.println(sMsg2);           
        }
    }

    public static void ciclosFor() {
        System.out.println("=============[Ciclos For ]==========================");
        System.out.println("For Incremental");
        for(int i = 0; i <= 10; i++) {
              System.out.println(i); 
        }
        System.out.println("For Decremental");
        for(int i = 10; i >= 0; i--) {
              System.out.println(i); 
        }
        System.out.println("For Incremental +2");

        for(int i = 0; i <= 10; i+=2) {
              System.out.println(i); 
        }
        System.out.println("For Decremental -2");
        for(int i = 10; i >= 0; i-=2) {
              System.out.println(i); 
        }
    }
    
    public static void Principal(int opcion) {
        System.out.println(opcion);
        switch(opcion)
        {
        case 1:
            ifSimpleyCompuesto();
             break; 
        case 2:
            ifOperadoreRelacionales();
             break; 
        case 3:
            ifOpRelacionalesIgualdad();
            break; 
        case 4:
            ifAnidadoExtendido();
            break; 
        case 5:
            ifAnidadoImplicito();
            break; 
        case 6:
            ciclosFor();
            break; 
        default : 
            System.out.println("Opción no válida");
        }        
    }

          
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        encabezado();
         cuerpo();
            // scanner
            // asignar variable 
             // Principal(6);
             
        // Imprimir la suma consecutiva del 0 al 10
        int suma = 0;
        for (int i = 0; i <=10; i++) {
            suma += i;
        }
        System.out.println(suma);
         
        // Calcular el factorial del 1 al 10
        int factorial;
        factorial = 1;
        for (int i = 1; i <= 10; i++) {
            //factorial = factorial + (factorial*i);
            factorial *=i;
        }
             
        pie();
    }
    
}
